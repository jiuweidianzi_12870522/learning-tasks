# 上次分析微服务变化
    
- 进程 和 线程
 1. 进程包含线程
 2. 线程由进程启动
 3. 线程之间资源共享：
    spring：注入对象，调用的对象方法
    
 4. 进程之间资源共享
     网络通讯


- CAP理论
一致性、      可用性、    网络分区容忍性


- 分布式事务


# 单体 与 微服务

http://www.broadview.com.cn/article/419498



# 落地

https://mp.weixin.qq.com/s?__biz=MzA3MDg4Nzc2NQ==&mid=2652136128&idx=1&sn=6d0d326e380a1a8a5d87a4667656bdcb&chksm=84d532fab3a2bbeceb40daec4a52d3cc51dc95831bd009ed79ec0fc99236b9358d25b4a94efd&scene=21#wechat_redirect

## 学习
### 业务开发
### 消息队列 （RabbitMQ, RocketMQ、 Kafaka）
### 缓存方向（redis（）   ， ehache(一级)）
### 部署方向（容器、K8s、其他工具、监控）
### 搜索方向  elasticSearch， Solr
### NoSql数据库  MongoDB


